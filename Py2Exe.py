# -*-coding:UTF-8 -*-
# Coded By SessizEr(Msf4) |||| Ar- Ge Kulübü |||| 01.08.2018
# Version 1.0
# PYTHON FİLE TO EXE FİLE ---->  Py2Exe
# Bu Programın çalışabilmesi için bilgisayarınızda cx_freeze adlı programın kurulu olması gerekmektedir.
# python --m pip install cx_Freeze

"""Additions"""
from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
import subprocess
import os

class App(object):

    """Constructive function"""
    def __init__(self):
        root.title("Py2Exe")
        root.geometry("350x400")
        root.resizable(width=False, height=False)
        root.iconbitmap("files/logoicon.ico")

        self._Convert = PhotoImage(file="files/convert.png")
        self.mainWindowBanner()
        self.mainWindowEntrys()
        self.mainWindowButtons()

    def mainWindowBanner(self):
        self._banner = PhotoImage(file = "files/py2exe.gif")
        self._pht = Label(image=self._banner)
        self._pht.image = self._banner
        self._pht.place(relx= -0.150,rely= -0.1)

    def mainWindowEntrys(self):
        """Upper Entry"""
        self._PythonPathEntry = Entry(width= 38)
        self._PythonFileEntry = Entry(width= 38)

        self._PythonPathEntry.place(relx=0.32, rely=0.21)
        self._PythonFileEntry.place(relx=0.32, rely=0.296)

        """Lower Entry"""
        self._ExeFileNameEntry = Entry(width=38)
        self._ExeFileLogoEntry = Entry(width=38)
        self._ExeFileDescEntry = Entry(width=35)
        self._ExeFileVersEntry = Entry(width=38)

        self._ExeFileNameEntry.place(relx= 0.3, rely= 0.453)
        self._ExeFileLogoEntry.place(relx=0.3, rely=0.553)
        self._ExeFileDescEntry.place(relx=0.35, rely=0.653)
        self._ExeFileVersEntry.place(relx=0.3, rely=0.753)

    def mainWindowButtons(self):
        """Upper Buttons"""
        self._PythonPath = Button(text="Python File Path",bg="silver",fg="darkred",
                                  cursor="hand2",overrelief="groove",font="Times 8 bold",
                                  command = self.OperationOne)
        self._PythonFile = Button(text="Your Python File",bg="silver",fg="darkred",
                                  cursor="hand2",overrelief="groove",font="Times 8 bold",
                                  command= self.OperationTwo)

        self._PythonPath.place(relx= 0.01, rely=0.2)
        self._PythonFile.place(relx= 0.01, rely=0.290)

        """Lower Buttons"""
        self._ExeFileNameBt = Button(text="Exe File Name",bg="silver",fg="royalblue",
                                     cursor="hand2",overrelief="groove",font="Times 8 bold",
                                     command=self.OperationFour)

        self._ExeFileLogoBt = Button(text="Exe File Logo",bg="silver",fg="royalblue",
                                     cursor="hand2",overrelief="groove",font="Times 8 bold",
                                     command=self.OperationThree)

        self._ExeFileDescBt = Button(text="Exe File Description",bg="silver",fg="royalblue",
                                     cursor="hand2",overrelief="groove",font="Times 8 bold",
                                     command=self.OperationFive)

        self._ExeFileVersBt = Button(text="Exe File Version",bg="silver",fg="royalblue",
                                     cursor="hand2",overrelief="groove",font="Times 8 bold",
                                     command=self.OperationSix)

        self._ExeFileNameBt.place(relx=0.01, rely=0.45)
        self._ExeFileLogoBt.place(relx=0.01, rely=0.55)
        self._ExeFileDescBt.place(relx=0.01, rely=0.65)
        self._ExeFileVersBt.place(relx=0.01, rely=0.75)

        """Convert Button"""
        self._Py2Exe = Button(image=self._Convert,
                              command=self.OperationFinal)
        self._Py2Exe.place(relx=0.430,rely=0.819)

    """Operations"""
    def OperationOne(self):
        self._optOne = self._PythonPath
        self._optOne = filedialog.askdirectory()
        self._PythonPathEntry.insert(END,self._optOne+"/python.exe")
        self._Pythonexe = self._PythonPathEntry.get()

    def OperationTwo(self):
        self._optTwo = self._PythonFile
        self._optTwo = filedialog.askopenfilename()
        self._PythonFileEntry.insert(END,self._optTwo)

    def OperationThree(self):
        self._optThree = self._ExeFileLogoBt
        self._optThree = filedialog.askopenfilename()
        self._ExeFileLogoEntry.insert(END,self._optThree)

    def OperationFour(self):
        self._optFour= self._ExeFileNameEntry.get()
        print(self._optFour)

    def OperationFive(self):
        self._optFive= self._ExeFileDescEntry.get()
        print(self._optFive)

    def OperationSix(self):
        self._optSix= self._ExeFileVersEntry.get()
        print(self._optSix)

    def OperationFinal(self):
        self._optOpen = open('setup.py','w')
        print(self._optOpen)

        self._optOpen.write(
"""
# -*-coding:cp1254 -*-
import os
import sys
from cx_Freeze import setup, Executable

os.environ['TCL_LIBRARY'] = '{}/tcl/tcl8.6'
os.environ['TK_LIBRARY'] =  '{}/tcl/tk8.6'


buildOptions = dict(
    packages = [],
    excludes = [],
    include_files = ['{}/DLLs/tcl86t.dll',
                     '{}/DLLs/tk86t.dll']
    )

base = None
if sys.platform == "win32":
    base = "Win32GUI"

setup(
    name ="{}",
    version = "{}", 
    description = "{}",
    options = dict(build_exe = buildOptions),
    executables = [Executable("{}",icon="{}", base=base)])
"""

.format(self._optOne,self._optOne,self._optOne,self._optOne,self._optFour,
        self._optSix,self._optFive,self._optTwo,self._optThree)
    )

        self._optOpen.close()

        cmd = subprocess.Popen('cmd.exe /K {}/python.exe setup.py build ' .format(self._optOne))


"""İdentifiers"""
root = Tk()
app = App()
root.mainloop()





























